import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { LinePage } from './line.page';
import { ActivatedRoute } from '@angular/router';
import { DataService } from '../../services/data.service';

import { MOCK } from '../../models/data';

describe('LinePage', () => {
    let component: LinePage;
    let fixture: ComponentFixture<LinePage>;
    const routeStub = {
        snapshot: {
            paramMap: {
                get: (name: string) => 'T1'
            }
        }
    };
    let route, dataServiceSpy, dataGetLineSpy, dataGetLineRankingsSpy, dataGetRawLineDataSpy;

    beforeEach(async(() => {
        dataGetLineSpy = of(MOCK.LINES.lines.find(line => line.lineId === 'T1'));
        dataGetLineRankingsSpy = of(MOCK.RANKINGS);
        dataGetRawLineDataSpy = of({
            samples: [{
                datetime: '2019-09-26',
                xAccel: ((Math.random() * 10) * (1 - 0.6)) + (0.1 * 0.6),
                yAccel: ((Math.random() * 10) * (1 - 0.6)) + (0.1 * 0.6),
                zAccel: ((Math.random() * 10) * (1 - 0.6)) + (0.1 * 0.6),
                gpsLat: 0,
                gpsLon: 0
            }]
        });
        dataServiceSpy = jasmine.createSpyObj('DataService', {
            getLine: dataGetLineSpy,
            getLineRankings: dataGetLineRankingsSpy,
            getRawLineData: dataGetRawLineDataSpy
        });

        TestBed.configureTestingModule({
            declarations: [ LinePage ],
            schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
            providers: [
                { provide: ActivatedRoute, useValue: routeStub },
                { provide: DataService, useValue: dataServiceSpy }
            ],
            imports: [
                HttpClientTestingModule
            ]
        })
        .compileComponents();
    }));

    beforeEach(async () => {
        fixture = await TestBed.createComponent(LinePage);
        component = fixture.componentInstance;
        route = TestBed.get(ActivatedRoute);
        await fixture.detectChanges();
    });

    it('should create the line page', () => {
        expect(component).toBeTruthy();
    });

    it('should get the line ID from the url param', () => {
        expect(route.snapshot.paramMap.get('id')).toEqual('T1', 'invalid line id');
    });

    it('should get the requested line', () => {
        expect(dataServiceSpy.getLine).toHaveBeenCalled();
    });

    it('should use cards', () => {
        const app = fixture.nativeElement;
        const tabItems = app.querySelectorAll('ion-card');
        expect(tabItems.length).toEqual(8);
    });

    it('should have charts', () => {
        const app = fixture.nativeElement;
        const chartItems = app.querySelectorAll('app-line-chart');
        const expectedTitles = ['Accelerometer X', 'Accelerometer Y', 'Accelerometer Z', 'Rankings'];
        expect(chartItems.length).toEqual(4);
        chartItems.forEach(chart => {
            expect(expectedTitles).toContain(chart.title);
        });
    });

    it('#rankingColour() should return the correct colour given different inputs', () => {
        const testData = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
        const min = 0;
        const max = 10;
        const third = Math.round((max - min) / 3);
        const twoThirds = Math.round(2 * (max - min) / 3);
        for (const value of testData) {
            if (value > twoThirds) {
                expect(component.rankingColour(value)).toEqual('green');
            } else if (value > third && value <= twoThirds) {
                expect(component.rankingColour(value)).toEqual('orange');
            } else {
                expect(component.rankingColour(value)).toEqual('red');
            }
        }
    });
});
