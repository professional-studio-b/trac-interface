import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription, timer } from 'rxjs';
import { DataService } from '../../services/data.service';

import { Line, Ranking } from '../../models/data';

@Component({
    selector: 'app-line',
    templateUrl: './line.page.html',
    styleUrls: ['./line.page.scss'],
})
export class LinePage implements OnInit, OnDestroy {

    /** Line ID being asked to be displayed */
    requestedId: string;
    /** The line object */
    line: Line;

    /** Returned line ranking objects */
    lineRankings: { date: string, value: number }[];

    /** Accelerometer data in X direction */
    accelerometerDataX: { date: string, value: number }[];
    /** Accelerometer data in Y direction */
    accelerometerDataY: { date: string, value: number }[];
    /** Accelerometer data in Z direction */
    accelerometerDataZ: { date: string, value: number }[];

    /** Backlog of accelerometer data in X direction */
    accelerometerDataXBacklog: { date: string, value: number }[];
    /** Backlog of accelerometer data in Y direction */
    accelerometerDataYBacklog: { date: string, value: number }[];
    /** Backlog of accelerometer data in Z direction */
    accelerometerDataZBacklog: { date: string, value: number }[];
    /** Timer */
    timerSubscription: Subscription;

    /**
     * Constructor for LinePage
     *
     * @param route Angular ActivatedRoute for router
     * @param dataService App data service
     */
    constructor(private route: ActivatedRoute, private dataService: DataService) {}

    /**
     * ngOnInit
     *
     * Run when component has been initialised
     */
    ngOnInit(): void {
        this.getLine();
    }

    ngOnDestroy(): void {
        this.timerSubscription.unsubscribe();
    }

    /**
     * getLine
     *
     * Get the requested line from service and populate charts with returned data
     */
    getLine(): void {
        this.requestedId = this.route.snapshot.paramMap.get('id');
        this.dataService.getLine(this.requestedId).subscribe(line => {
            if (line) {
                this.line = line;

                const from = new Date();
                const fortnite = from.getDate() - 14;
                from.setDate(fortnite);
                this.dataService.getLineRankings(this.line.lineId, from.toISOString()).subscribe(response => {
                    this.lineRankings = response.rankings.map(sample => {
                        return {
                            date: sample.date,
                            value: sample.rank
                        };
                    });
                });

                this.accelerometerDataX = new Array(50);
                this.accelerometerDataY = new Array(50);
                this.accelerometerDataZ = new Array(50);
                this.accelerometerDataXBacklog = new Array(50);
                this.accelerometerDataYBacklog = new Array(50);
                this.accelerometerDataZBacklog = new Array(50);

                const timeSub = timer(20, 2000); // Interval set for 2 second
                this.timerSubscription = timeSub.subscribe(t => this.updateCharts());
            }
        });
    }

    /**
     * updateCharts
     *
     * Update data for charts
     */
    async updateCharts(): Promise<void> {
        return new Promise((resolve) => {
            this.dataService.getRawLineData(this.line.lineId).subscribe(response => {
                this.accelerometerDataXBacklog = response.samples.map(sample => {
                    return {
                        date: sample.datetime,
                        value: sample.xAccel
                    };
                }).reverse();
                this.accelerometerDataYBacklog = response.samples.map(sample => {
                    return {
                        date: sample.datetime,
                        value: sample.yAccel
                    };
                }).reverse();
                this.accelerometerDataZBacklog = response.samples.map(sample => {
                    return {
                        date: sample.datetime,
                        value: sample.zAccel
                    };
                }).reverse();

                this.accelerometerDataX = response.samples.map(sample => {
                    return {
                        date: sample.datetime,
                        value: sample.xAccel
                    };
                }).slice(response.samples.length - 50);
                this.accelerometerDataY = response.samples.map(sample => {
                    return {
                        date: sample.datetime,
                        value: sample.yAccel
                    };
                }).slice(response.samples.length - 50);
                this.accelerometerDataZ = response.samples.map(sample => {
                    return {
                        date: sample.datetime,
                        value: sample.zAccel
                    };
                }).slice(response.samples.length - 50);

                resolve();
            });
        });
    }

    /**
     * rankingColour
     *
     * Return colour as a string based on data severity
     *
     * @param value number being checked against
     * @returns string Colour value ('red' | 'orange' | 'green')
     */
    rankingColour(value: number): string {
        const min = 0;
        const max = 10;
        const third = Math.round((max - min) / 3);
        const twoThirds = Math.round(2 * (max - min) / 3);
        return value > twoThirds ? 'green' : value > third && value <= twoThirds ? 'orange' : 'red';
    }

    sampleTimes(backlog: { date: string, value: number }[]): string[] {
        return backlog.map(sample => {
            const date = new Date(sample.date);
            return (
                date.getHours()
                + ':' + (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes())
                + ':' + (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds())
            );
        });
    }

    sampleDates(backlog: { date: string, value: number }[]): string[] {
        return backlog.map(sample => {
            const date = new Date(sample.date);
            return date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
        });
    }

    sampleValues(backlog: { date: string, value: number }[]): number[] {
        return backlog.map(sample => +sample.value);
    }

}
