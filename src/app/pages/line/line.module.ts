import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { LinePage } from './line.page';

import { ChartsModule } from '../../components/charts.module';

const routes: Routes = [
    {
        path: '',
        component: LinePage
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        ChartsModule
    ],
    declarations: [
        LinePage
    ]
})
export class LinePageModule {}
