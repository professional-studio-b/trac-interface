import { Component, OnInit } from '@angular/core';

import { DataService } from '../../services/data.service';

import { Line } from '../../models/data';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.page.html',
    styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

    lines: Line[] = [];

    /**
     * Constructor for DashboardPage
     *
     * @param dataService App data service
     */
    constructor(private dataService: DataService) {}

    /**
     * ngOnInit
     *
     * Run when component has been initialised
     */
    ngOnInit(): void {
        this.dataService.getLines().subscribe(response => {
            response.lines.forEach(line => {
                this.lines.push(line);
            });
        });
    }

    /**
     * rankingColour
     *
     * Return colour as a string based on data severity
     *
     * @param value number being checked against
     * @returns string Colour value ('red' | 'orange' | 'green')
     */
    rankingColour(value: number): string {
        const min = 0;
        const max = 10;
        const third = Math.round((max - min) / 3);
        const twoThirds = Math.round(2 * (max - min) / 3);
        return value > twoThirds ? 'green' : value > third && value <= twoThirds ? 'orange' : 'red';
    }

}
