import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { DashboardPage } from './dashboard.page';

describe('DashboardPage', () => {
    let component: DashboardPage;
    let fixture: ComponentFixture<DashboardPage>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ DashboardPage ],
            schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
            imports: [
                HttpClientTestingModule
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DashboardPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create the dashboard', () => {
        expect(component).toBeDefined();
    });

    it('should have a title', () => {
        const app = fixture.nativeElement;
        const title = app.querySelector('ion-title');
        expect(title.textContent).toEqual('TRAC Dashboard');
    });
});
