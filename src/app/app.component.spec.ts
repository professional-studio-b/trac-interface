import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TestBed, ComponentFixture, async } from '@angular/core/testing';
import { of } from 'rxjs';

import { Platform } from '@ionic/angular';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { AppComponent } from './app.component';
import { DataService } from './services/data.service';

import { MOCK } from './models/data';

describe('AppComponent', () => {
    let component: AppComponent;
    let fixture: ComponentFixture<AppComponent>;

    let platformSpy, platformReadySpy, platformIsSpy, dataServiceSpy, dataGetLinesSpy;

    beforeEach(async(() => {
        platformReadySpy = Promise.resolve();
        platformIsSpy = Platform.prototype.is('mobile');
        platformSpy = jasmine.createSpyObj('Platform', { ready: platformReadySpy, is: platformIsSpy });

        dataGetLinesSpy = of(MOCK.LINES);
        dataServiceSpy = jasmine.createSpyObj('DataService', { getLines: dataGetLinesSpy });

        TestBed.configureTestingModule({
            declarations: [ AppComponent ],
            schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
            providers: [
                { provide: Platform, useValue: platformSpy },
                { provide: DataService, useValue: dataServiceSpy }
            ],
            imports: [
                RouterTestingModule.withRoutes([]),
                HttpClientTestingModule
            ],
        }).compileComponents();
    }));

    beforeEach(async () => {
        fixture = await TestBed.createComponent(AppComponent);
        component = fixture.componentInstance;
        await fixture.detectChanges();
    });

    it('should create the app', async () => {
        expect(component).toBeTruthy();
    });

    it('should initialise the app', async () => {
        expect(platformSpy.ready).toHaveBeenCalled();
        await platformReadySpy;
        expect(dataServiceSpy.getLines).toHaveBeenCalled();
    });

    it('should have menu labels', () => {
        const app = fixture.nativeElement;
        const menuItems = app.querySelectorAll('ion-label');
        expect(menuItems.length).toEqual(1 + MOCK.LINES.lines.length);
        expect(component.appPages.length).toEqual(1 + MOCK.LINES.lines.length);
        expect(menuItems[0].textContent).toContain('Dashboard', 'Dashboard button doesn\'t exist');
        MOCK.LINES.lines.forEach((line, index) => {
            expect(menuItems[index + 1].textContent).toContain(line.lineId, 'Line button missing');
        });
    });

    it('should have routes', () => {
        const app = fixture.nativeElement;
        const menuItems = app.querySelectorAll('ion-item');
        expect(menuItems.length).toEqual(1 + MOCK.LINES.lines.length);
        expect(menuItems[0].getAttribute('ng-reflect-router-link')).toEqual('/dashboard', 'Dashboard route link doesn\'t exist');
        MOCK.LINES.lines.forEach((line, index) => {
            expect(menuItems[index + 1].getAttribute('ng-reflect-router-link'))
                .toEqual('/line/' + line.lineId, 'Line route link doesn\'t exist');
        });
    });

});
