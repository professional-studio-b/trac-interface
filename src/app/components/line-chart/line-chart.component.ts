import { Component, ViewChild, Input, AfterViewInit, OnChanges } from '@angular/core';
import { Chart } from 'chart.js';

@Component({
    selector: 'app-line-chart',
    templateUrl: './line-chart.component.html',
    styleUrls: ['./line-chart.component.scss'],
})
export class LineChartComponent implements AfterViewInit, OnChanges {

    /** ChartJS line chart component */
    @ViewChild('lineCanvas', { static: false }) lineCanvas: any;
    /** ChartJS line chart */
    lineChart: Chart;

    /** Chart title */
    @Input() title: string;
    /** Chart data */
    @Input() data: number[];
    /** Data minimum value */
    @Input() min: number;
    /** Data maximum value */
    @Input() max: number;
    /** Flip colours */
    @Input() flip: boolean;
    /** Label prefix */
    @Input() labels: string[];

    /**
     * Constructor for LineChartComponent
     */
    constructor() {}

    /**
     * ngAfterViewInit
     *
     * Run after view has been initialised
     */
    ngAfterViewInit(): void {
        if (this.data) {
            this.createChart();
        } else {
            console.warn('No data passed to chart');
        }
    }

    /**
     * ngOnChanges
     *
     * Run every time a change is detected in the component's inputs
     */
    ngOnChanges(): void {
        if (this.lineChart) {
            this.lineChart.data.datasets.forEach(dataset => {
                dataset.data = this.data;
            });
            this.lineChart.data.labels = this.labels;
            this.lineChart.update();
        }
    }

    /**
     * createChart
     *
     * Create ChartJS chart
     */
    createChart(): void {
        this.lineChart = new Chart(this.lineCanvas.nativeElement, {
            type: 'line',
            data: {
                labels: this.labels ? this.labels : this.defaultLabels(this.data.length),
                datasets: [
                    {
                        label: this.title,
                        fill: false,
                        backgroundColor: 'grey',
                        borderColor: 'grey',
                        borderWidth: 0.5,
                        pointBorderColor: this.flip ? this.colourFlipped : this.colourNorm,
                        pointBackgroundColor: this.flip ? this.colourFlipped : this.colourNorm,
                        pointHoverBackgroundColor: this.flip ? this.colourFlipped : this.colourNorm,
                        pointHoverBorderColor: this.flip ? this.colourFlipped : this.colourNorm,
                        pointHoverBorderWidth: 2,
                        pointRadius: 4,
                        pointHitRadius: 10,
                        data: this.data,
                        spanGaps: false
                    }
                ]
            },
            options: {
                display: true,
                elements: {
                    line: {
                        tension: 0 // disables bezier curves
                    }
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            min: this.min,
                            max: this.max
                        }
                    }]
                },
                legend: {
                    display: false
                },
                title: {
                    display: true,
                    text: this.title
                }
            }
        });
    }

    /**
     * colourNorm
     *
     * Return colour as a string based on data severity
     *
     * @param context Chart context object
     * @returns string Colour value ('red' | 'orange' | 'green')
     */
    colourNorm(context: any): string {
        const min = context.chart.options.scales.yAxes[0].ticks.min;
        const max = context.chart.options.scales.yAxes[0].ticks.max;
        const value = Math.abs(context.dataset.data[context.dataIndex]);
        const third = (max - min) / 3;
        const twoThirds = 2 * (max - min) / 3;
        return value > twoThirds ? 'green' : value > third && value <= twoThirds ? 'orange' : 'red';
    }

    /**
     * colourFlipped
     *
     * Same as colourNorm, but flipped output
     *
     * @param context Chart context object
     * @returns string Colour value ('red' | 'orange' | 'green')
     */
    colourFlipped(context: any): string {
        const min = context.chart.options.scales.yAxes[0].ticks.min;
        const max = context.chart.options.scales.yAxes[0].ticks.max;
        const value = Math.abs(context.dataset.data[context.dataIndex]);
        const third = (max - min) / 3;
        const twoThirds = 2 * (max - min) / 3;
        return value > twoThirds ? 'red' : value > third && value <= twoThirds ? 'orange' : 'green';
    }

    /**
     * defaultLabels
     *
     * Generate an array of labels for the chart's x-axis
     *
     * @param numLabels Number of labels being displayed
     * @returns string[] Generated labels
     */
    defaultLabels(numLabels: number): string[] {
        const labels = [];
        for (let count = numLabels - 1; count >= 0; --count) {
            labels.push(`time-${count}`);
        }
        return labels;
    }

}
