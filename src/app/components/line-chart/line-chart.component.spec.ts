import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LineChartComponent } from './line-chart.component';

describe('LineChartComponent', () => {
    let component: LineChartComponent;
    let fixture: ComponentFixture<LineChartComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ LineChartComponent ],
            schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LineChartComponent);
        component = fixture.componentInstance;
        component.title = 'Test Chart';
        component.data = [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 ];
        component.labels = [ '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' ];
        component.min = 0;
        component.max = 10;
    });

    it('should create the component', () => {
        fixture.detectChanges();
        expect(component).toBeTruthy();
    });

    it('should not display a chart if there is no data', () => {
        component.data = null;
        fixture.detectChanges();
        const app = fixture.nativeElement;
        const canvas = app.querySelector('canvas');
        expect(canvas).toBeFalsy();
    });

    it('should create the chart if there is data', () => {
        fixture.detectChanges();
        const app = fixture.nativeElement;
        const canvas = app.querySelector('canvas');
        expect(canvas).toBeTruthy();
        expect(component.lineChart).toBeTruthy();
        expect(component.lineChart.config.type).toEqual('line', 'Incorrect chart type');
    });

    it('should update the chart on data change', () => {
        fixture.detectChanges();
        const currentData = component.data;
        expect(component.lineChart.data.datasets[0].data).toEqual(currentData);
        const newData = [1, 2, 1, 2, 9, 9, 9, 7, 8, 9];
        component.data = newData;
        component.ngOnChanges();
        fixture.detectChanges();
        expect(component.data).toEqual(newData);
        expect(component.lineChart.data.datasets[0].data).toEqual(newData);
    });

    it('#defaultLabels() should return the correct x axis labels', () => {
        fixture.detectChanges();
        expect(component.defaultLabels(5)).toEqual(['time-4', 'time-3', 'time-2', 'time-1', 'time-0']);
    });

    it('#colourNorm() should return the correct colour given different inputs', () => {
        fixture.detectChanges();
        const testData = [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ];
        const chartStub = {
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            min: 0,
                            max: 10
                        }
                    }]
                }
            }
        };
        for (let i = 0; i < testData.length; ++i) {
            if (testData[i] > (2 * (component.max - component.min) / 3)) {
                expect(component.colourNorm({ dataset: { data: testData }, dataIndex: i, chart: chartStub })).toEqual('green');
            } else if (
                testData[i] > ((component.max - component.min) / 3)
                && testData[i] <= (2 * (component.max - component.min) / 3)
            ) {
                expect(component.colourNorm({ dataset: { data: testData }, dataIndex: i, chart: chartStub })).toEqual('orange');
            } else {
                expect(component.colourNorm({ dataset: { data: testData }, dataIndex: i, chart: chartStub })).toEqual('red');
            }
        }
    });

    it('#colourFlipped() should return the correct colour given different inputs', () => {
        fixture.detectChanges();
        const testData = [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ];
        const chartStub = {
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            min: 0,
                            max: 10
                        }
                    }]
                }
            }
        };
        for (let i = 0; i < testData.length; ++i) {
            if (testData[i] > (2 * (component.max - component.min) / 3)) {
                expect(component.colourFlipped({ dataset: { data: testData }, dataIndex: i, chart: chartStub })).toEqual('red');
            } else if (
                testData[i] > ((component.max - component.min) / 3)
                && testData[i] <= (2 * (component.max - component.min) / 3)
            ) {
                expect(component.colourFlipped({ dataset: { data: testData }, dataIndex: i, chart: chartStub })).toEqual('orange');
            } else {
                expect(component.colourFlipped({ dataset: { data: testData }, dataIndex: i, chart: chartStub })).toEqual('green');
            }
        }
    });
});
