import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';

import { DataService } from './services/data.service';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss']
})
export class AppComponent {
    /** Pages to list in sidebar */
    public appPages = [
        {
            title: 'Dashboard',
            url: '/dashboard',
            icon: 'analytics'
        }
    ];

    /**
     * Constructor for AppComponent
     *
     * @param platform Ionic Angular Platform
     * @param dataService App data service
     */
    constructor(private platform: Platform, private dataService: DataService) {
        this.initialiseApp();
    }

    /**
     * initialiseApp
     *
     * Initialise pages and routes when platform is ready
     */
    initialiseApp(): void {
        this.platform.ready().then(() => {
            this.dataService.getLines().subscribe(response => {
                response.lines.forEach(line => {
                    this.appPages.push({
                        title: line.lineId,
                        url: `/line/${line.lineId}`,
                        icon: 'train'
                    });
                });
            });
        });
    }
}
