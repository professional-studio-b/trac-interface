export interface Ranking {
    date: string;
    rank: number;
}

export interface Rankings {
    rankings: Ranking[];
}

export interface Line {
    lineId: string;
    rank: Ranking;
}

export interface Lines {
    lines: Line[];
}

export interface DataPoint {
    datetime: string;
    xAccel: number;
    yAccel: number;
    zAccel: number;
    gpsLat: number;
    gpsLon: number;
}

export interface Samples {
    samples: DataPoint[];
}

export const MOCK: {
    LINES: Lines,
    RANKINGS: Rankings,
    SAMPLES: Samples
} = {
    LINES: {
        lines: [
            { lineId: 'T1', rank: { date: '2019-09-26', rank: Math.round(Math.random() * 10) } },
            { lineId: 'T2', rank: { date: '2019-09-26', rank: Math.round(Math.random() * 10) } },
            { lineId: 'T3', rank: { date: '2019-09-26', rank: Math.round(Math.random() * 10) } },
            { lineId: 'T4', rank: { date: '2019-09-26', rank: Math.round(Math.random() * 10) } },
            { lineId: 'T5', rank: { date: '2019-09-26', rank: Math.round(Math.random() * 10) } },
            { lineId: 'T6', rank: { date: '2019-09-26', rank: Math.round(Math.random() * 10) } },
            { lineId: 'T7', rank: { date: '2019-09-26', rank: Math.round(Math.random() * 10) } },
            { lineId: 'T8', rank: { date: '2019-09-26', rank: Math.round(Math.random() * 10) } },
            { lineId: 'T9', rank: { date: '2019-09-26', rank: Math.round(Math.random() * 10) } }
        ]
    },
    RANKINGS: {
        rankings: [
            { date: '2019-09-16', rank: Math.round(Math.random() * 10) },
            { date: '2019-09-17', rank: Math.round(Math.random() * 10) },
            { date: '2019-09-18', rank: Math.round(Math.random() * 10) },
            { date: '2019-09-19', rank: Math.round(Math.random() * 10) },
            { date: '2019-09-20', rank: Math.round(Math.random() * 10) },
            { date: '2019-09-21', rank: Math.round(Math.random() * 10) },
            { date: '2019-09-22', rank: Math.round(Math.random() * 10) },
            { date: '2019-09-23', rank: Math.round(Math.random() * 10) },
            { date: '2019-09-24', rank: Math.round(Math.random() * 10) },
            { date: '2019-09-25', rank: Math.round(Math.random() * 10) },
            { date: '2019-09-26', rank: Math.round(Math.random() * 10) }
        ]
    },
    SAMPLES: {
        samples: [
            {
                datetime: '2019-09-26',
                xAccel: ((Math.random() * 3) * (1 - 0.6)) + (0.1 * 0.6),
                yAccel: ((Math.random() * 3) * (1 - 0.6)) + (0.1 * 0.6),
                zAccel: ((Math.random() * 3) * (1 - 0.6)) + (0.1 * 0.6),
                gpsLat: 0,
                gpsLon: 0
            }
        ]
    }
};
