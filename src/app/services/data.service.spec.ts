import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { DataService } from './data.service';

describe('DataService', () => {
    let service: DataService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [ DataService ],
            imports: [
                HttpClientTestingModule
            ]
        });
        service = TestBed.get(DataService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    // it('#getLines() should return observable array', (done: DoneFn) => {
    //     service.getLines().subscribe(value => {
    //         expect(value).toBeTruthy();
    //         done();
    //     });
    // });

    // it('#getLine() should return observable object', (done: DoneFn) => {
    //     service.getLine('T1').subscribe(value => {
    //         expect(value).toBeTruthy();
    //         done();
    //     });
    // });

    // it('#getLineRankings() should return observable array', (done: DoneFn) => {
    //     service.getLineRankings('T1').subscribe(value => {
    //         expect(value).toBeTruthy();
    //         done();
    //     });
    // });

    // it('#getRawLineData() should return observable array', (done: DoneFn) => {
    //     service.getRawLineData('T1').subscribe(value => {
    //         expect(value).toBeTruthy();
    //         done();
    //     });
    // });
});
