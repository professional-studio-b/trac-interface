import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { environment } from '../../environments/environment';

import { Rankings, Line, Lines, Samples, MOCK } from '../models/data';

@Injectable({
    providedIn: 'root'
})
export class DataService {

    constructor(private http: HttpClient) {}

    getLines(): Observable<Lines> {
        // return of(MOCK.LINES).pipe(
        return this.http.get<Lines>(environment.api + '/lines').pipe(
            catchError(this.handleError<Lines>('getLines', { lines: [] }))
        );
    }

    getLine(id: string): Observable<Line> {
        // return of(MOCK.LINES).pipe(
        return this.http.get<Lines>(environment.api + '/lines').pipe(
            map(response => response.lines.find(line => line.lineId === id)),
            catchError(this.handleError<Line>('getLine', null))
        );
    }

    getLineRankings(id: string, from = '', to = ''): Observable<Rankings> {
        const params = new HttpParams().set('from', from).set('to', to);
        // return of(MOCK.RANKINGS).pipe(
        return this.http.get<Rankings>(environment.api + '/lines/' + id, { params }).pipe(
            catchError(this.handleError<Rankings>('getLineRankings', { rankings: [] }))
        );
    }

    getRawLineData(id: string, from = '', to = ''): Observable<Samples> {
        const params = new HttpParams().set('from', from).set('to', to);
        // return of(MOCK.SAMPLES).pipe(
        return this.http.get<Samples>(environment.api + '/lines/' + id + '/raw', { params }).pipe(
            catchError(this.handleError<Samples>('getRawLineData', { samples: [] }))
        );
    }

    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T>(operation, result?: T) {
        return (error: any): Observable<T> => {
            console.error(`${operation} failed: ${error.message}`, error);
            return of(result as T);
        };
    }
}
