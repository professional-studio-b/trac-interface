# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.4.0] - 2019-10-27
### Changed
- Use new API gateway
- Using RXJS Subscriptions for polling API
- Charts use date and/or times as x-axis labels
- Accelerometer y-axis range

## [0.3.0] - 2019-10-22
### Added
- Improved Dashboard
- More unit tests
- More CI stages

### Changed
- Service functionality to use API Endpoint
- UI improvements

## [0.2.0] - 2019-10-09
### Added
- Line page
- More unit tests
- Data service
- Ranking charts and summaries

### Changed
- Moved live data line charts to Line pages

## [0.1.1] - 2019-09-11
### Added
- Unit testing and linting framework using Karma, TSLint and GitLab CI/CD

## [0.1.0] - 2019-09-10
### Added
- Basic charts using chart.js
- Chart colour coding and inputs

## [0.0.2] - 2019-09-09
### Added
- New README file with development instructions
- Data page template 

## [0.0.1] - 2019-09-05
### Added
- Ionic interface
