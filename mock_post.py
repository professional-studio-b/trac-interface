import requests
from random import random
from datetime import datetime
from time import sleep
import json

api = "ql6d9v0jw8.execute-api.ap-southeast-2.amazonaws.com"
collect_route = "https://" + api + "/trac/collect"


def random_accelerometer():
    return ((random() * 3) * (1 - 0.6)) + (0.1 * 0.6)


if __name__ == "__main__":
    for _i in range(100):
        date = datetime.utcnow().isoformat() + 'Z'
        ranX = str(random_accelerometer())
        ranY = str(random_accelerometer())
        ranZ = str(random_accelerometer())

        data = [{
            "dateTime": date,
            "xAccel": ranX,
            "yAccel": ranY,
            "zAccel": ranZ,
            "gpsLat": 0,
            "gpsLon": 0
        }]

        payload = {
            "lineId": "T1",
            "samples": data
        }

        headers = {
            'Content-Type': "application/json",
            'Accept': "*/*",
            'Cache-Control': "no-cache",
            'Accept-Encoding': "gzip, deflate",
            'Content-Length': "304",
            'Connection': "keep-alive",
            'cache-control': "no-cache"
        }

        response = requests.request("POST", collect_route, data=json.dumps(payload), headers=headers)

        print(response.text)
        sleep(1)
