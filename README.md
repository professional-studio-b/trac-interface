# TRAC Interface
[![deployment link](https://img.shields.io/badge/deployment-production-blue)](http://professional-studio-b-apps3bucket-18g389iy2z24t.s3-website-ap-southeast-2.amazonaws.com)
[![pipeline status](https://gitlab.com/professional-studio-b/trac-interface/badges/master/pipeline.svg)](https://gitlab.com/professional-studio-b/trac-interface/commits/master)
[![coverage report](https://gitlab.com/professional-studio-b/trac-interface/badges/master/coverage.svg)](https://gitlab.com/professional-studio-b/trac-interface/commits/master)

TRAC user interface web application using Angular with Chart.js and Ionic.

## Development

Please read `CONTRIBUTING.md` before starting development on this application. Below is a brief overview for quickly getting started.

### Prerequisites

Make sure the following tools are installed and up to date:

- node / npm
- git
- An editor (e.g. VSCode or Atom)

### Quickstart

Follow these instructions to quickly get the app up and running. Read the following sections for more details.

1. Clone the git repository
2. Navigate into the newly created project folder
3. Run an `npm install` to fetch the dependencies
4. Serve the app with `npm start`

### Using Git

This project is hosted on GitLab, meaning we will be using git as our version control tool.
Make sure you have access to the GitLab Group before continuing.

### Project structure

The project follows the Angular project directory structure. Almost all development will be done in the src/ folder, particularly in the src/app/ folder.

### GitLab Testing Pipline

Whenever a commit is made to the repository, GitLab will automatically run tests based on the configuration in the .gitlab-ci.yml file.
This primarily involves linting and unit testing. Merge requests can only be merged into master if the testing pipeline runs successfully.

### Resources

Links I used to set up testing:

- Installing Puppeteer - <https://www.digestibledevops.com/devops/2018/09/18/puppeteer-with-angular.html>
- Installing Ubuntu ChromeHeadless dependencies - <https://github.com/GoogleChrome/puppeteer/issues/3443#issuecomment-433096772>
